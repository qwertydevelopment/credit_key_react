import Info from '../../../assets/images/Info.svg';
import Method_icon from '../../../assets/images/method_icon.svg';
import Star from '../../../assets/images/star.svg';
import Vendor from '../../../assets/images/vendor.svg';
import QuestionMark from '../../../assets/images/question_mark.svg';
import amazon from '../../../assets/images/amazon.svg';
import nike from '../../../assets/images/nike.svg';
import apple from '../../../assets/images/apple.svg';

import Card from '../../shared/Card';

import { BaseGrid, BaseGridItem } from '../../layout/base-grid/BaseGrid';
import Box from '../../shared/Box';
import Table from '../../shared/Table';
import { useState } from 'react';

const tableOptions = {
  columnDefs: [
    {
      headerName: 'Merchant',
      field: 'merchant',
      cellRenderer: (params) => {
        return (
          <>
            <img src={params.merchantIcon} alt={params.merchant} />
            <div>{params.merchant}</div>
          </>
        );
      },
    },
    {
      headerName: 'Date',
      field: 'date',
    },
    {
      headerName: 'Order Id',
      field: 'orderId',
    },
    {
      headerName: 'Term',
      field: 'term',
    },
    {
      headerName: 'Total Amt',
      field: 'totalAmt',
    },
    {
      headerName: 'Status',
      field: 'status',
    },
    {
      headerName: 'Next Payment Date',
      field: 'nextPaymentDate',
    },
  ],
};

const MOCK_ROW_DATA = [
  {
    id: 1,
    merchant: 'Amazon',
    date: '3 Sep',
    merchantIcon: amazon,
    orderId: '#243523',
    term: '90d',
    totalAmt: '$1,105.50',
    status: 'Active',
    nextPaymentDate: '11/08/2021',
  },
  {
    id: 2,
    merchant: 'Nike',
    merchantIcon: nike,
    date: '12 Sep',
    orderId: '#243523',
    term: '90d',
    totalAmt: '$1,105.50',
    status: 'Active',
    nextPaymentDate: '11/08/2021',
  },
  {
    id: 3,
    merchant: 'Apple',
    merchantIcon: apple,
    date: '16 Sep',
    orderId: '2435232',
    term: '90d',
    totalAmt: '$12222',
    status: 'Active',
    nextPaymentDate: '11/23/1992',
  },
];

function PageLayout() {
  const [rowData, setRowData] = useState(MOCK_ROW_DATA);

  return (
    <div className="pb-6 md:pb-9">
      <BaseGrid>
        {/* Statment Card */}
        <BaseGridItem>
          <div className="md:row-span-2 md:col-span-2">
            <Card>
              {{
                header: {
                  title: 'Statment',
                },
                content: (
                  <>
                    {/* <!-- Credits --> */}
                    <div className="grid grid-cols-2">
                      {/* <!-- Approved credit--> */}
                      <div>
                        <div className="font-avenir-black text-xl md:text-2xl">
                          $12,456.00
                        </div>
                        <div className="flex items-center mt-1">
                          <div className="mr-1 font-avenir-book text-black text-opacity-60 text-sm">
                            Approved Credit
                          </div>
                          <img src={Info} alt="info" />
                        </div>
                      </div>
                      {/* <!-- Remaining credit--> */}
                      <div>
                        <div className="font-avenir-black text-xl md:text-2xl text-positive">
                          $12,456.00
                        </div>
                        <div className="flex items-center mt-1">
                          <div className="mr-1 font-avenir-book text-black text-opacity-60 text-sm">
                            Remaining Credit
                          </div>
                          <img src={Info} alt="info" />
                        </div>
                      </div>
                    </div>
                    {/* <!-- Rate --> */}
                    <div className="py-3 -mx-4 md:-mx-8 mt-3 md:mt-6 border-t border-b border-black border-opacity-5 ">
                      <div className="flex justify-between px-8">
                        <div className="flex items-center mt-1">
                          <div className="mr-1 font-avenir-book text-black text-opacity-60 text-sm">
                            Base Rate
                          </div>
                          <img src={Info} alt="info" />
                        </div>
                        <div className="font-avenir-black text-xl md:text-2xl">
                          1%
                        </div>
                      </div>
                    </div>
                    <div className="bg-primary bg-secondary bg-warning"></div>
                  </>
                ),
                footer: (
                  <div className="p-4 md:p-8 flex items-center justify-between">
                    <div className="font-avenir-book text-black text-opacity-60 text-sm pr-1">
                      Learn How you can increase limit
                    </div>
                    <div className="font-avenir-black text-primary bg-white rounded-lg text-sm px-4 py-2 cursor-pointer flex-shrink-0">
                      Increase Limit
                    </div>
                  </div>
                ),
              }}
            </Card>
          </div>
        </BaseGridItem>

        {/* Order Car */}
        <BaseGridItem>
          <div className="md:col-span-2">
            <Card>
              {{
                header: {
                  title: 'Orders',
                  action: 'View All',
                  actionHandler: () => {
                    console.log('CLICK');
                  },
                },
                content: (
                  <>
                    <div className="flex items-center justify-between mt-4 md:mt-8">
                      {/* <!-- Active --> */}
                      <div className="flex items-center justify-between flex-grow">
                        <div className="text-black text-opacity-60 text-sm">
                          Active
                        </div>
                        <div className="font-avenir-black text-base">20</div>
                      </div>
                      {/* <!-- Divider --> */}
                      <div className="border-l border-black border-opacity-5 mx-2 md:mx-8">
                         
                      </div>
                      {/* <!-- Finished --> */}
                      <div className="flex items-center justify-between flex-grow">
                        <div className="text-black text-opacity-60 text-sm">
                          Finished
                        </div>
                        <div className="font-avenir-black text-base">15</div>
                      </div>
                    </div>
                  </>
                ),
              }}
            </Card>
          </div>
        </BaseGridItem>

        {/* Payment Methods Car */}
        <BaseGridItem>
          <div className="md:col-span-2">
            <Card>
              {{
                header: {
                  title: 'Primary Payment Method',
                  action: 'View All',
                  actionHandler: () => {
                    console.log('CLICK');
                  },
                },
                content: (
                  <>
                    <div className="flex items-center justify-between mt-4 md:mt-7">
                      <div className="flex items-center">
                        <img src={Method_icon} alt="method" />
                        <div className="text-black text-opacity-60 text-sm mx-6">
                          Chase Bank
                        </div>
                        <div className="text-black text-opacity-60 text-sm">
                          ****1212
                        </div>
                      </div>

                      <div className="text-positive text-sm">Active</div>
                    </div>
                  </>
                ),
              }}
            </Card>
          </div>
        </BaseGridItem>

        <BaseGridItem>
          <div className="md:col-span-2 lg:col-span-1">
            <Box>
              {{
                type: 'warning',
                icon: Vendor,
                title: 'Special offer to take our debit card',
                submit: {
                  label: 'Submit New invoice',
                  handler: () => console.log('SUBMITED'),
                },
              }}
            </Box>
          </div>
        </BaseGridItem>

        <BaseGridItem>
          <div className="md:col-span-2 lg:col-span-1">
            <Box>
              {{
                type: 'secondary',
                icon: QuestionMark,
                title: 'Need Support?',
                submit: {
                  label: 'Check out our FAQs for common question',
                  handler: () => console.log('SUBMITED'),
                },
              }}
            </Box>
          </div>
        </BaseGridItem>

        <BaseGridItem>
          <div className="md:col-span-full lg:col-span-2">
            <Box>
              {{
                type: 'primary',
                icon: Star,
                title: 'Special offer to take our debit card',
                submit: {
                  label: 'Check out our FAQs for common question',
                  handler: () => console.log('SUBMITED'),
                },
              }}
            </Box>
          </div>
        </BaseGridItem>

        <BaseGridItem>
          <div className="col-span-full">
            <Table options={tableOptions} rowData={rowData} />
          </div>
        </BaseGridItem>
      </BaseGrid>
    </div>
  );
}

export default PageLayout;
