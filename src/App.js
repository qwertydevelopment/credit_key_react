// Layout
import SideNav from './components/layout/sideNav/SideNav';
import Container from './components/layout/container/Container';
import Header from './components/layout/header/Header';
import Scrollable from './components/layout/scrollable/Scrollable';

// Pages
import Dashboard from './components/pages/dashboard/Dashboard';

function App() {
  return (
    <div className="h-screen overflow-hidden font-avenir-book grid grid-cols-[auto_1fr]">
      <SideNav />

      <Container utilClasses="pt-4 md:pt-9 grid grid-rows-[auto_1fr] overflow-hidden">
        <Header />

        <Scrollable>
          {/* Pages goes here (React Router) */}
          <Dashboard />
        </Scrollable>
      </Container>
    </div>
  );
}

export default App;
