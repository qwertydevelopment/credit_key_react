const showHide = (condition) => {
  return [
    'transition-opacity',
    {
      'opacity-100': condition,
      'opacity-0': !condition,
    },
  ];
};

export { showHide };
