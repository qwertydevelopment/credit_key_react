import React from 'react';
import ReactDOM from 'react-dom';

// Global CSS
import './index.css';

// Root Component
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);
